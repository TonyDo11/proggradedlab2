import vehicles.Bicycle;
// Tony Do 2239932
public class BikeStore {
	public static void main(String[]args) {
		Bicycle[] bicycles = new Bicycle[4];
		
		bicycles[0] = new Bicycle("Windbreaker", 4, 255);
		bicycles[1] = new Bicycle("Nike", 2, 100);
		bicycles[2] = new Bicycle("Bionicle", 5, 345);
		bicycles[3] = new Bicycle("Lego", 2, 32);
		
		for(Bicycle bike : bicycles) {
			System.out.println(bike);
		}
	}
}