package vehicles;
// Tony Do 2239932
public class Bicycle {
   String manufacturer;
   int numberGears;
   double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public String toString() {
        return "Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", MaxSpeed: " + maxSpeed;
    }
}
